exports.ondemand = function(service, text, o){
	if ($verboseLog) { Ti.API.info("FluorNet: ondemand() - input service = "+service+", text = "+text); };
	
	if(Titanium.Network.networkType == Titanium.Network.NETWORK_NONE){
		if ($verboseLog) { Ti.API.info("FluorNet: ondemand() - No Network"); };
		if (o.error) { o.error("No Network"); };
		return;
	}
	
	var xhr = Titanium.Network.createHTTPClient({
		onload: function() {
			if ($verboseLog) { Ti.API.info("FluorNet: ondemand() - createHTTPClient: onload, responseText = "+this.responseText); };
			if (o.success) { o.success(this.responseText); };		
		},
		onerror: function(e) {
			if ($verboseLog) { Ti.API.info("FluorNet: ondemand() - createHTTPClient: onerror = "+e.error); };
			//Ti.Analytics.featureEvent('app.createHTTPClientError.getCompany', {error: e.error});
			if (o.error) {
				if(service==="lookupbylastname"){
					o.error("Error with lookup API");
				} else if(service==="exactsearch"){
					o.error("Error with exact lookup API");
				}  else {
					o.error("Unknown error with network.js exports.ondemand");
				}
			};
		},
		timeout: 10000,
	});
	
	var items = Alloy.Globals.itemsPerSearch;
	
	if(service==="lookupbylastname"){
		if ($verboseLog) { Ti.API.info("FluorNet: ondemand() - createHTTPClient - send() lookupbylastnameweblookupbylastname"); };
		xhr.open("GET", Alloy.Globals.sourceURL + "/lookupbylastname?systemcolumns=0x0000&count=" + items + "&keys="+text);
	} else if(service==="exactsearch"){
		if ($verboseLog) { Ti.API.info("FluorNet: ondemand() - createHTTPClient - send() quote"); };
		xhr.open("GET", Alloy.Globals.sourceURL + "/lookupbylastname?systemcolumns=0x0000&count=" + items + "&keysexactmatch=true&keys="+text);
	}
	
	xhr.send();
};