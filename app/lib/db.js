/**
 * Fluor Phones database functions
 * Copied from fluorhseconstruction @author Adam Morciszek, Jeff Weyant
 * Modified/Rewrite for fluorphones 2015-10-01 Ron Johnston
 * TODO:  revamp the whole check/open/close handling and where calls are made:  all within this file or also from flrLoadFTD
 * TODO:  wrap all db.execute function calls in try/catch eg as used in getresultset
 */

exports.checkDB = function(db,o){
	// Create the DB or check if available so we can pick up where we left off
	// TODO: If we just check/open the DB from flrLoadFTD, then just return dbObj;
	if ($verboseLog) { Ti.API.info("DB: checkDB - start"); };
	
	var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
		
	setupDB(dbObj);
	dbObj.close();

	if ($verboseLog) { Ti.API.info("DB: checkDB - end"); };

  	if (o.success) { o.success(null);}
 };

function setupDB(db) {
	// Create table of contacts:  FTDContacts
	/* Columns:
	 * "docUNID" CHAR PRIMARY KEY NOT NULL, 
	 * "LNAME" CHAR, 
	 * "FNAME" CHAR, 
	 * "PHONE_NO" CHAR, 
	 * "OFFICE" CHAR, 
	 * "LOC" CHAR, 
	 * "IODC" CHAR, 
	 * "FAX_No" CHAR, 
	 * "ModifiedDate" CHAR, 
	 * "UpdatedBy" CHAR
	 */
	// TODO: After DB is in place/working:  try 'WITHOUT ROWID' at the end of the execute to see if it's more performant
	if ($verboseLog) { Ti.API.info("DB: setupDB - start"); };
	db.execute('CREATE TABLE if NOT EXISTS FTDContacts ("docUNID" CHAR PRIMARY KEY NOT NULL, "LNAME" CHAR, "FNAME" CHAR, "PHONE_NO" CHAR, "OFFICE" CHAR, "LOC" CHAR, "IODC" CHAR, "FAX_No" CHAR, "ModifiedDate" CHAR, "UpdatedBy" CHAR)');

	if ($verboseLog) { Ti.API.info("DB: setupDB - end"); };
}; 

/* TODO - Clean up 
 * input record set */ 
exports.testInsert = function() {
	// test insert in FTDContacts table, then print contents of the table
	if ($verboseLog) { Ti.API.info("DB: testInsert - start"); };
	var utils = require('utilities');
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	db.execute("INSERT INTO FTDContacts (docUNID, LNAME, FNAME, PHONE_NO, OFFICE, LOC, IODC, FAX_No, ModifiedDate, UpdatedBy) VALUES (?,?,?,?,?,?,?,?,?,?)", utils.createguid(),"Johnston","Ron","1-864-281-4871","Greenville","Kernersville","20","",Date(),"RonJ");
	db.close();
	var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT * from FTDContacts");
	printTable(rs);			
	rs.close();
	if ($verboseLog) { Ti.API.info("DB: testInsert - end"); };
};


/* TODO - Delete after testing 
 * FOR TESTING: SHOW RESULT SET IN CONSOLE
 */
function printTable(rs)
{
	if ($verboseLog) { Ti.API.info("DB: printTable - start"); };
	
	var cols = rs.fieldCount;
	var fields = "";
	var y = 0;  //assume at least 1 field
	fields = rs.fieldName(y);
	y++;
	while (y < cols) { 
			fields = fields + "," + rs.fieldName(y);										
			 y++;
	}
	Ti.API.info("Fields");
	Ti.API.info(fields);
	
	var x = 0;
	var data = "";
	while (rs.isValidRow())
	{
		var y = 0;
		//data = x;  //row counter
		data = rs.fieldName(y);  //assume at least 1 column
		y++;
		while (y < cols) { 
			data = data + "," +  rs.fieldByName(rs.fieldName(y));							
			 y++;
		}
		rs.next();
		x++;
		Ti.API.info(data);
		data = "";
	}
	if ($verboseLog) { Ti.API.info("DB: printTable - start"); };
};

function closeDB(dbName)
{
	var db = Ti.Database.open(dbName);
	db.close(); 
};

/**
 Create index for table viev 
 */
exports.createIndex = function(data)
{
	var fChar = '';
	var rows = data.length;
	var tblIndex = [];
	var rowno = 0;
	for (var i=0; i<rows; i++){
		if (fChar != data[i].title.charAt(0)){
			fChar = data[i].title.charAt(0);
			tblIndex[rowno] = {title:fChar,index:i};
			rowno++;
		}
	}
	return tblIndex;
};


function emptyRow(){
	if (OS_IOS)
	{
			return {title:''};
	}		

	if (OS_ANDROID)
	{	
		return Ti.UI.createPickerRow({title:''});
	}
};

/**
 Returns result Set of Contractors 
 */
exports.getContractors = function()
{
	var rs = getResultSet('hse_contract', "SELECT OBJECTDES from Sheet1 order by OBJECTDES");
	var data = [];
	var x = 0;
	var fChar = '';
	
	//add empty row
	data[x] = emptyRow();
	x++;	

	while (rs.isValidRow())
	{
		var contr = rs.fieldByName('OBJECTDES');
	//	Ti.API.info(contr);
		if (OS_IOS)
		{
			if (fChar != contr.charAt(0))
			{
				fChar = contr.charAt(0);
				data[x] = {title:contr, header:fChar};
			} else {
				data[x] = {title:contr};
			}
		}		

		if (OS_ANDROID)
		{	
			data[x] = Ti.UI.createPickerRow({title:contr});
		}
		x++;
		rs.next();
	}
	closeDB('hse_contract');
	return data;
};

/* return a result set set */
function getResultSet(dbName, sql)
{
	if ($verboseLog) { Ti.API.info("DB: getResultSet - start"); };
	try {
		var db = Ti.Database.open(dbName);	
		var rs = db.execute(sql);
		
		//db.close();
	} catch (err) { 
			Ti.API.error(err);
	} finally { 
			//db.close(); 
	}
	
	if ($verboseLog) { Ti.API.info("DB: getResultSet - end"); };
	return rs;
};