/* 
 * Reload the local data store from the live Fluor Telephone Directory
 * Copied from fluorhseconstruction @author Adam Morciszek, Jeff Weyant
 * Modified/Rewrite for fluorphones 2015-10-01 Ron Johnston
 * TODO:  revamp the whole check/open/close handling and where calls are made:  all within this file or also from flrLoadFTD
 * TODO:  wrap all db.execute function calls in try/catch eg as used in getresultset
 */
var args = arguments[0] || {};
// var DB = require('db');

exports.reloadAll = function(){
	if (Alloy.Globals.verboseLog) { Ti.API.info("FluorNet: reloadAll - Check for network"); };
	
	if (Titanium.Network.networkType == Titanium.Network.NETWORK_NONE) {
		if (Alloy.Globals.verboseLog) { Ti.API.info("FluorNet: ondemand() - No Network"); };
		if (o.error) { o.error("No Network"); };
		return;
	}
	
	// TODO: Update following skeleton plan for the process
	// Steps to load DB
	// Check if we are just starting or restarting in the middle
	// If just starting, blah blah
	// If restarting, check the record count to be sure it matches the current location
	// If the count does not match, restart from the beginning
	
	// Check if 'FluorTelDir' is available
	// TODO: For now, just create one DB.  When that works, then replace with rotate between 2 DBs, active/new.  Create new, rename/replace active after success.
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - Check for/create SQLite DB"); };
	
	// was this:  DB.checkDB(Alloy.Globals.DatabaseName, {
	var checkDBResult = checkDB(Alloy.Globals.DatabaseName, {
		success: function(e) {
			if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - DB check ok"); };
		},
		error: function(e) {
			Ti.API.error(e);
			if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - DB check failed"); };
			return;
		}
	});
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - start test insert"); };
	// TEST Insert a row and print the results
	/*
	 * DISABLED THE INSERT TEST.  WORKS.
	var testInsertResult = testInsert(Alloy.Globals.DatabaseName, {
		success: function(e) {
			if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - test insert ok"); };
		},
		error: function(e) {
			Ti.API.error(e);
			if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - test insert failed"); };
			return ("Error");
		}
	});
	 */
	
	// Get data and populate the table
	// START LOOP HERE
	var xhr = Titanium.Network.createHTTPClient({
		// TODO:  CHECK STATUS OF THE GET/SEND FROM XHR HERE FOR ASYNCH OPERATION
		onload: function() {
			// this function is called when data is returned from the server and available for use
    	    // this.responseText holds the raw text return of the message (used for text/JSON)
			if (Alloy.Globals.verboseLog) { Ti.API.info("reloadAll - createHTTPClient GET: success"); };
			// Received the data, now insert in the table
			var insertResponse = insertRecords(this.responseText);
		},
		onerror: function(e) {
			// this function is called when an error occurs, including a timeout
			if (Alloy.Globals.verboseLog) { Ti.API.info("reloadAll - createHTTPClient GET: error " + e.Error); };
			// any action here?
			return ("Error retrieving data: " + e.Error);
		},
		timeout: 10000,
	});
		
	// TODO:  First get minimum 30 or so, then ramp up to get all with buffer start/count entries
	var items = Alloy.Globals.itemsPerSearch;
	if (Alloy.Globals.verboseLog) { Ti.API.info("reloadAll - createHTTPClient - sending GET"); };
	// TODO:  Check why lookup stopped working on dev10
	// prepare to send the request
	
	// iOS Load TESTS COMPLETE.  TEST DOWNLOAD ALL 33K RECORDS AT ONCE ON ANDROID, 
	var restURL = Alloy.Globals.sourceURL + "/lookupbylastname?systemcolumns=0x0000&start=10000&count=1000";
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("*** flrLoadFTD.js[reloadAll function]:  .js: restURL:" + restURL); };
	xhr.open("GET", restURL);
	// send the request
	xhr.send();

	// END LOOP HERE


 
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: reloadAll - FINISHED"); };
	// TODO:  Do we need the following return???  best practice on ending functions......
	return;
};

exports.commitData = function(e){
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - start"); };
	
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	// Change the Domino provided '@entryid' to a variable name we can use ('g' is global replace)
	var responseString = JSON.parse(e.replace(/@entryid/g,'entryid'));
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - record to insert: " + responseString.length); };
	
	var len = responseString.length;
	
	if(responseString.Message || len===0){
		if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - no records or error received"); };
		return ("No records to process");
	}
	
	// TODO: Add the record modified date to the REST provider
	var dateNowTemp = new Date();
	var dateStringTemp = dateNowTemp.toGMTString();
	
	// TODO: Add the record modified by to the REST provider
	var modifiedbyTemp = "James Leyte";
	db.execute('BEGIN');
	// Now add the records. We checked len above and know it's not 0
	for(i=0;i<len;i++){
		//Can be valid to not have a first name
		// if ((responseString[i].LNAME.length > 0) && (responseString[i].PHONE_NO.length > 0) ) {
			// var entryidTemp = responseString[i].entryid;
			// var lnameTemp = responseString[i].LNAME;
			// var fnameTemp = responseString[i].FNAME;
			// var phoneTemp = responseString[i].PHONE_NO;
			// var officeTemp = responseString[i].Office;
			// var locTemp = responseString[i].LOC;
			// var iodcTemp = responseString[i].IODC;
			// var faxTemp = responseString[i].FAX_No;
		 if ((responseString[i].COMP_LNAME.length > 0) && (responseString[i].COMP_PHONE_NO.length > 0) ) {
			 var entryidTemp = responseString[i].entryid;
			 var lnameTemp = responseString[i].COMP_LNAME;
			 var fnameTemp = responseString[i].COMP_FNAME;
			 var phoneTemp = responseString[i].COMP_PHONE_NO;
			 var officeTemp = responseString[i].COMP_Office;
			 var locTemp = responseString[i].COMP_LOC;
			 var iodcTemp = responseString[i].COMP_IODC;
			 var faxTemp = responseString[i].COMP_FAX_No;
			
			// Modify/clean data as needed here
			var fullnameTemp = fnameTemp + " " + lnameTemp;
	
			if (entryidTemp.indexOf("-") == -1) {
				var docUNIDTemp = entryidTemp;
			}
			else {
				var docUNIDTemp = entryidTemp.substring(entryidTemp.indexOf("-") + 1, entryidTemp.length);
			}
			
			docUNIDTemp = i + '_' + docUNIDTemp;
			
			try {
				db.execute("INSERT INTO FTDContacts (docUNID, LNAME, FNAME, PHONE_NO, OFFICE, LOC, IODC, FAX_No, FullName, ModifiedDate, ModifiedBy) VALUES (?,?,?,?,?,?,?,?,?,?,?)", docUNIDTemp, lnameTemp, fnameTemp, phoneTemp, officeTemp, locTemp, iodcTemp, faxTemp, fullnameTemp, dateStringTemp, modifiedbyTemp);
			} catch (err) { 
				Ti.API.error(err);
			} finally {
	
			}
		}
	}
	db.execute('COMMIT');
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - inserted records: " + i++); };
	db.close();

	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - end"); };
	//alert("finished with loadall");
	//Ti.App.fireEvent('refresh-data', null);	
};

exports.getRefreshDate = function() {
	var rows = getResultSet(Alloy.Globals.DatabaseName, "SELECT value FROM Parameters where param_name = 'RefreshDate'");
	var refreshDate = '';
	if (rows.isValidRow()) {
		Ti.API.info("flrLoadFTD: getRefreshDate: " + rows.field(0));
		refreshDate = rows.field(0);
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: getRefreshDate: " + refreshDate); };
	rows.close();
	closeDB(Alloy.Globals.DatabaseName);
	return refreshDate;
};

exports.saveRefreshDate = function(e){
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: saveRefreshDate - start"); };
	
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	if(e.length>0){
		db.execute('BEGIN');
		db.execute("DELETE FROM Parameters where param_name = 'RefreshDate'");
		db.execute("INSERT INTO Parameters (param_name, value) VALUES (?,?)", 'RefreshDate', e);
		db.execute('COMMIT');
		if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: saveRefreshDate: " + e); };
	}
	
	db.close();

	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: saveRefreshDate - end"); };
};

function insertRecords(e){
	// TODO - Clean up 
	// Takes the REST/JSON formatted records passed in and adds them to the FTDContacts table
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - start"); };
	
//	var utils = require('utilities');  TODO: REMOVE THIS 
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	// Change the Domino provided '@entryid' to a variable name we can use ('g' is global replace)
	var responseString = JSON.parse(e.replace(/@entryid/g,'entryid'));
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - record count: " + responseString.length); };
	
	var len = responseString.length;
	
	if(responseString.Message || len===0){
		if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - no records or error received"); };
		return ("No records to process");
	}
	
	// TODO: Add the record modified date to the REST provider
	var dateNowTemp = new Date();
	var dateStringTemp = dateNowTemp.toGMTString();
	
	// TODO: Add the record modified by to the REST provider
	var modifiedbyTemp = "James Leyte";

	// Now add the records. We checked len above and know it's not 0
	for(i=0;i<len;i++){
		// var entryidTemp = responseString[i].entryid;
		// var lnameTemp = responseString[i].LNAME;
		// var fnameTemp = responseString[i].FNAME;
		// var phoneTemp = responseString[i].PHONE_NO;
		// var officeTemp = responseString[i].Office;
		// var locTemp = responseString[i].LOC;
		// var iodcTemp = responseString[i].IODC;
		// var faxTemp = responseString[i].FAX_No;
		 var lnameTemp = responseString[i].COMP_LNAME;
		 var fnameTemp = responseString[i].COMP_FNAME;
		 var phoneTemp = responseString[i].COMP_PHONE_NO;
		 var officeTemp = responseString[i].COMP_Office;
		 var locTemp = responseString[i].COMP_LOC;
		 var iodcTemp = responseString[i].COMP_IODC;
		 var faxTemp = responseString[i].COMP_FAX_No;
		
		// Modify/clean data as needed here
		var fullnameTemp = fnameTemp + " " + lnameTemp;
					
		// Example entryid: '94-5B77FD810A0FBAAD862578DA00421389'
		// Remove the initial number and dash
		if (entryidTemp.indexOf("-") == -1) {
			var docUNIDTemp = entryidTemp;
		}
		else {
			var docUNIDTemp = entryidTemp.substring(entryidTemp.indexOf("-") + 1, entryidTemp.length);
		}
//		Ti.API.info("docUNIDTemp is: " + docUNIDTemp);  // TODO:  REMOVE THIS

		try {
			db.execute("INSERT INTO FTDContacts (docUNID, LNAME, FNAME, PHONE_NO, OFFICE, LOC, IODC, FAX_No, FullName, ModifiedDate, ModifiedBy) VALUES (?,?,?,?,?,?,?,?,?,?,?)", docUNIDTemp, lnameTemp, fnameTemp, phoneTemp, officeTemp, locTemp, iodcTemp, faxTemp, fullnameTemp, dateStringTemp, modifiedbyTemp);
			//db.close();
		} catch (err) { 
			Ti.API.error(err);
		} finally {
			// do this regardless of success/failure
			//db.close(); 
		}
	}
	
	db.close();
	
	// TEMP PRINT ALL RECORDS IN THE TABLE.  TODO:  REMOVE THIS.   NOTE:  get all or get a few using one of the selects below
//	var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT * from FTDContacts");
	//var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT * from FTDContacts LIMIT 30 OFFSET 33000");
//	var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT * from FTDContacts LIMIT 30");
	//printTable(rs);			
	//rs.close();
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: insertRecords - end"); };
	alert("finished with loadall");
	Ti.App.fireEvent('refresh-data', null);
};

exports.deleteAll = function() {
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: deleteAll - start"); };
	
	var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: deleteAll - delete all data"); };
	dbObj.execute("DELETE FROM FTDContacts");
	dbObj.execute("DELETE FROM Parameters where param_name = 'RefreshDate'");
	dbObj.close();
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: deleteAll - end"); };
};

exports.chkDB = function() {
	
	return;
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: chkDB - start"); };
	
	var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: chkDB - setup db"); };
	// Create table in the DB, passing the db object
	setupDB(dbObj);
		
	var rows = dbObj.execute("SELECT * FROM FTDContacts");
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: checkDB - rows in table:" + rows.rowCount); };
	
	/* ALTERNATE FOR TESTING:  DELETE ALL ROWS AND QUIT. .....   DELETE AND CONTINUE TO RELOAD ON ANDROID PHONE.........  TESTING. */
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: chkDB - delete all data"); };
	dbObj.execute("DELETE FROM FTDContacts");
	dbObj.execute("DELETE FROM Parameters where param_name = 'RefreshDate'");

	dbObj.close();

	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: chkDB - end"); };
};

function checkDB(db,o){
	return;
	// Create the DB or check if available so we can pick up where we left off
	// TODO: If we just check/open the DB from flrLoadFTD, then just return dbObj;
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: checkDB - start"); };
	
	var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: checkDB - setup db"); };
	// Create table in the DB, passing the db object
	setupDB(dbObj);
		
	// TODO:  DECIDE WHEN/WHERE TO DELETE ....  FOR NOW, DELETE HERE <<<<<<<<<<<<<<<<<<<<<<<    DELETING THE DATABASE COMPLETELY
	//dbObj.remove();
	
	//dbObj = null;
		
	// NOW REOPEN.... use this when creating a new instance of the db
	//var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	// for testing
	var rows = dbObj.execute("SELECT * FROM FTDContacts");
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: checkDB - rows in table:" + rows.rowCount); };
	
	/* ALTERNATE FOR TESTING:  DELETE ALL ROWS AND QUIT. .....   DELETE AND CONTINUE TO RELOAD ON ANDROID PHONE.........  TESTING. */
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: checkDB - delete all data"); };
	dbObj.execute("DELETE FROM FTDContacts");
//	Return;

	dbObj.close();

	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: checkDB - end"); };

  	if (o.success) { o.success(null); };
 };

// TODO: Change this to close a db based on the object with added error handling or DELETE IT
function closeDB(dbName){
	var db = Ti.Database.open(dbName);
	db.close(); 
};

function setupDB(db) {
	// Create table of contacts:  FTDContacts
	/* Columns:
	 * "docUNID" CHAR PRIMARY KEY NOT NULL, 
	 * "LNAME" CHAR, 
	 * "FNAME" CHAR, 
	 * "PHONE_NO" CHAR, 
	 * "OFFICE" CHAR, 
	 * "LOC" CHAR, 
	 * "IODC" CHAR, 
	 * "FAX_No" CHAR,   
	 * "FullName" CHAR, 
	 * "ModifiedDate" CHAR, 
	 * "ModifiedBy" CHAR
	 */
	// TODO: After DB is in place/working:  try 'WITHOUT ROWID' at the end of the execute to see if it's more performant
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: setupDB - start"); };
	
	db.execute('CREATE TABLE if NOT EXISTS FTDContacts ("docUNID" TEXT PRIMARY KEY, "LNAME" TEXT, "FNAME" TEXT, "PHONE_NO" TEXT, "OFFICE" TEXT, "LOC" TEXT, "IODC" TEXT, "FAX_No" TEXT, "FullName" TEXT, "ModifiedDate" TEXT, "ModifiedBy" TEXT)');
	db.execute('CREATE TABLE if NOT EXISTS UsedContacts ("docUNID" TEXT PRIMARY KEY, "DateLastUsed" TEXT)');

	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: setupDB - end"); };
}; 

/* TODO - Clean up 
 * input record set */ 
function testInsert(){
	// test insert in FTDContacts table, then print contents of the table
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: testInsert - start"); };
	
	var utils = require('utilities');
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	// DON'T DO THIS FOR NOW.  IT'S TESTED/WORKS
//	db.execute("INSERT INTO FTDContacts (docUNID, LNAME, FNAME, PHONE_NO, OFFICE, LOC, IODC, FAX_No, FullName, ModifiedDate, ModifiedBy) VALUES (?,?,?,?,?,?,?,?,?,?,?)", utils.createguid(),"Johnston","Ron","1-864-281-4871","Greenville","Kernersville","20","","Ron Johnston",Date(),"RonJ");
	db.close();
	var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT * from FTDContacts");
	printTable(rs);			
	rs.close();
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: testInsert - end"); };
};

/* TODO - Delete after testing 
 * FOR TESTING: SHOW RESULT SET IN CONSOLE
 */
function printTable(rs){
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: printTable - start"); };
	
	var cols = rs.fieldCount;
	var fields = "";
	var y = 0;  //assume at least 1 field
	fields = rs.fieldName(y);
	y++;
	while (y < cols) { 
			fields = fields + "," + rs.fieldName(y);										
			 y++;
	}
	Ti.API.info("Fields");
	Ti.API.info(fields);
	
	var x = 0;
	var data = "";
	while (rs.isValidRow())
	{
		var y = 0;
		// data = x;  //row counter
		// assume at least 1 column
		data = rs.fieldByName(rs.fieldName(y));  
		y++;
		while (y < cols) { 
			data = data + "," +  rs.fieldByName(rs.fieldName(y));							
			 y++;
		}
		rs.next();
		x++;
		Ti.API.info(data);
		data = "";
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: printTable - start"); };
};

/* return a result set */
function getResultSet(dbName, sql){
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: getResultSet - start"); };
	try {
		var db = Ti.Database.open(dbName);
		var rs = db.execute(sql);
  		//db.close();
	} catch (err) { 
			Ti.API.error(err);
	} finally { 
			//db.close(); 
	}
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: getResultSet - end"); };
	return rs;
};

exports.fetchUsers = function() {
	
	var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT docUNID, LNAME, FNAME, PHONE_NO, LOC, OFFICE, IODC, FAX_No from FTDContacts order by LNAME, FNAME");
	var data = [];
	var x = 0;
		
	while (rs.isValidRow())
	{
		data[x] = {UNID:rs.fieldByName('docUNID'), LNAME:rs.fieldByName('LNAME'), FNAME:rs.fieldByName('FNAME'), PHONE_NO:rs.fieldByName('PHONE_NO'), LOC:rs.fieldByName('LOC'), OFFICE:rs.fieldByName('OFFICE'), IODC:rs.fieldByName('IODC'), FAX_No:rs.fieldByName('FAX_No')};
		x++;
		rs.next();
		/* changed to 300 max.  TODO: Decide if this enough. */
		if (x > 300) {break;}
	}
	rs.close();
	closeDB(Alloy.Globals.DatabaseName);
	//if (x == 0) { data[x] = {LNAME:'Directory is empty', FNAME:'', PHONE_NO:'Click Refresh Now to download data'}; };	
	return data;
};

exports.fetchUsersByName = function(name) {
	var fSep=name.indexOf(",");

	//Account for apostrophes in SQL statement by doubling them
	var strRegExp = /'/g;
	name = name.replace(strRegExp, "''");

	if (fSep < 0) {
		fSep=name.indexOf(" ");
	}
	if (fSep < 0) {
		//no separators, check in entire string
		var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT docUNID, LNAME, FNAME, PHONE_NO, LOC, OFFICE, IODC, FAX_No from FTDContacts where FullName like \'%" + name + "%\' order by LNAME, FNAME");	
	} 
	else {
		var firstName = name.substring(0,fSep);
		var lSep=name.lastIndexOf(" ");
		if (lSep < 0) {
			lSep=name.lastIndexOf(",");
		}
		var lastName = name.substring(lSep+1);

		var strSQL = "SELECT docUNID, LNAME, FNAME, PHONE_NO, LOC, OFFICE, IODC, FAX_No from FTDContacts ";
		strSQL += "where (";
		strSQL += "(FName like \'%" + firstName + "%\' and LName like \'%" + lastName + "%\') ";
		strSQL += "    or (FName like \'%" + lastName + "%\' and LName like \'%" + firstName + "%\') ";
		// in case using multiple words for one name (ie. De Los Santos)
		strSQL += "    or (FullName like \'%" + name + "%\') ";
		strSQL += ") ";
		strSQL += "order by LNAME, FNAME";
		if (Alloy.Globals.verboseLog) { Ti.API.info("strSQL = " + strSQL); };
		var rs = getResultSet(Alloy.Globals.DatabaseName, strSQL);
	}
	
	var data = [];
	var x = 0;
	
	data[x] = rs.rowCount;
		
	while (rs.isValidRow())
	{

		data[x] = {UNID:rs.fieldByName('docUNID'), LNAME:rs.fieldByName('LNAME'), FNAME:rs.fieldByName('FNAME'), PHONE_NO:rs.fieldByName('PHONE_NO'), LOC:rs.fieldByName('LOC'), OFFICE:rs.fieldByName('OFFICE'), IODC:rs.fieldByName('IODC'), FAX_No:rs.fieldByName('FAX_No')};
		x++;
		rs.next();
		/* changed to 300 max.  TODO: Decide if this enough. */
		if (x > 300) {break;}
	}
	rs.close();
	closeDB(Alloy.Globals.DatabaseName);
	//if (x == 0) { data[x] = {LNAME:'Directory is empty', FNAME:'', PHONE_NO:'Click Refresh Now to download data'}; };	
	return data;
};

exports.getDirectoryCount = function() {
	var rows = getResultSet(Alloy.Globals.DatabaseName, "SELECT count(*) FROM FTDContacts");
	var count = 0;
	if (rows.isValidRow()) {
		Ti.API.info("flrLoadFTD: getDirectoryCount - rows.count:" + rows.field(0));
		count = rows.field(0);
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: getDirectoryCount - rows in table:" + count); };
	rows.close();
	closeDB(Alloy.Globals.DatabaseName);
	//if (x == 0) { data[x] = {LNAME:'Directory is empty', FNAME:'', PHONE_NO:'Click Refresh Now to download data'}; };	
	//return 100;
	return count;
};

exports.getLastUsed = function() {
	
	//var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT docUNID, LNAME, FNAME, PHONE_NO, LOC, OFFICE FROM FTDContacts WHERE docUNID in (SELECT docUNID from UsedContacts) order by LNAME, FNAME");
	var rs = null;
	var data = [];
	var x = 0;
		
	return data;
	
	while (rs.isValidRow())
	{
		data[x] = {UNID:rs.fieldByName('docUNID'), LNAME:rs.fieldByName('LNAME'), FNAME:rs.fieldByName('FNAME'), PHONE_NO:rs.fieldByName('PHONE_NO'), LOC:rs.fieldByName('LOC'), OFFICE:rs.fieldByName('OFFICE'), IODC:rs.fieldByName('IODC'), FAX_No:rs.fieldByName('FAX_No')};
		x++;
		rs.next();
		/* changed to 300 max.  TODO: Decide if this enough. */
		if (x > 300) {break;}
	}
	rs.close();
	closeDB(Alloy.Globals.DatabaseName);
	//if (x == 0) { data[x] = {LNAME:'Directory is empty', FNAME:'', PHONE_NO:'Click Refresh Now to download data'}; };	
	return data;
};

exports.UpdateLastUsed = function(UNID) {
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: save contact to UsedContacts - start"); };
	var rs = getResultSet(Alloy.Globals.DatabaseName, "SELECT docUNID from UsedContacts where docUNID = '" + UNID +"'");
		
	if (rs.rowCount==0)
	{
		if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: save contact to UsedContacts - no entry - do insert"); };
		var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
		var moment = require('alloy/moment');
		var now = moment().unix();
		dbObj.execute('INSERT INTO UsedContacts ("docUNID", "DateLastUsed") VALUES (?,?)', UNID, now);
	}
	rs.close();
	closeDB(Alloy.Globals.DatabaseName);
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: save contact to UsedContacts - end"); };

};

exports.deleteLastUsed = function () {
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: delete last used - start"); };
	
	var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: delete UserContact data"); };

	dbObj.execute("DELETE FROM UsedContacts");
	dbObj.close();

	if (Alloy.Globals.verboseLog) { Ti.API.info("flrLoadFTD: delete last used - end"); };

};
