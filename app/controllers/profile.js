/**
 * Fluor Phones: Copied originally from Appcelerator sample app
 * Modified: 2015-10-01 Ron Johnston
 * 
 * @overview
 * This is the controller file for the Profile View. The Contact Profile displays 
 * information passed in from the Directory View
 *
 * @copyright
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 *
 * @license
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

/**
 * Instantiate the variables assocaited with this controller
 */
var _args = arguments[0] || {};

/**
 * Check for passed in properties of the contact, and update the 
 * Label text and ImageView image values as required
 */
$.firstName.text = _args.FNAME;
$.lastName.text = _args.LNAME;
var tempPhone = _args.PHONE_NO;
$.phoneNumber.text = tempPhone.replace(/ /g,"-");
$.office.text = _args.OFFICE;
$.location.text = _args.LOC;
// TODO:  UNCOMMENT THE FOLLOWING WHEN THEY ARE AVAILABLE
$.iodc.text = _args.IODC;
$.fax.text = _args.FAX_No;

var loadData = require('flrLoadFTD');
loadData.UpdateLastUsed(_args.UNID);

/**
 * MOBILEWEB : In order to override the standard button style in the Navigation Bar, we will create our own
 * view to use
 */
if(OS_MOBILEWEB){
	var backBtn = Ti.UI.createLabel({
		text:"\uf104 Back",
		color: "#1F467D",
		font:{
			fontFamily:"icomoon",
			fontSize:20
		}
	});
	backBtn.addEventListener("click", function(e){
		Alloy.Globals.Navigator.navGroup.close($.profile);
	});
	$.profile.leftNavButton = backBtn;
}

function callContact(){	
	/**
	 * Opening a url of 'tel:phone#' (or 'telprompt:phone# for iOS) sends the phone# to the platform to make the phone call.
	 * The call will not be initiated; only displayed in the dialer for the user to take action.
	 */
	if(OS_IOS){
		var urlPhone = "tel:" + $.phoneNumber.text;	
	}
	else {
		var urlPhone = "tel:" + $.phoneNumber.text;	
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("Sending # to dialer: " + urlPhone); };
   	Ti.Platform.openURL(urlPhone);
}

/**
 * Closes the Window
 */
function closeWindow(){
	$.profile.close();
}

/**
 * fade in after the view rendered
 */
$.profile.addEventListener("postlayout", function(e){
	$.profile.animate({
		opacity: 1.0,
		duration: 250,
		curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
	});
});

