//  DECLERATIONS ============================
var args = arguments[0] || {};

//FUNCTIONS =================================
function downloadData(e){
	var strMessage = "You are about to download the Fluor Phonebook onto your device.  This can take a few minutes, depending on network connectivity. \n\nDo you wish to proceed?";
	var alertDialog = Titanium.UI.createAlertDialog({ 
			title: 'Confirm Refresh', 
			message: strMessage, 
			buttonNames: ['OK','CANCEL'] 
			}); 
	 	 
		if (Alloy.Globals.verboseLog) { Ti.API.info("Download directory: prompt to refresh"); };
	  	alertDialog.addEventListener('click', function(e){ 
       	if(e.index == 0) {    	
			Alloy.Globals.Navigator.open("downloader", {displayHomeAsUp:true});
       	}
       else {       
           //alert('go back to your app');           
       }
    }) ;
	 
	alertDialog.show();	
}

/**
 * Closes the Window
 */
function closeWindow(){
	$.rsawin.close();
}

//CODE START =============================
$.rsaWebView.url=Alloy.Globals.webViewURL;