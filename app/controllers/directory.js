/**
 *                              _                _             
 *                             | |              | |            
 *    __ _ _ __  _ __   ___ ___| | ___ _ __ __ _| |_ ___  _ __ 
 *   / _` | '_ \| '_ \ / __/ _ \ |/ _ \ '__/ _` | __/ _ \| '__|
 *  | (_| | |_) | |_) | (_|  __/ |  __/ | | (_| | || (_) | |   
 *   \__,_| .__/| .__/ \___\___|_|\___|_|  \__,_|\__\___/|_|   
 *        | |   | |                                            
 *        |_|   |_|  
 *      
 *      
 * @overview
 * This is the controller file for the Directory View. The directory view loads data from 
 * a flat file, and derives a Sectioned and Indexed (iOS) ListView displaying all contacts.
 * The Directory has two ListView Templates, one for standard contacts, the other to denote
 * that you have a marked the contact as a Bookmark (or Favorite). Also, the Directory View
 * can be filtered so that it only displays bookmarked or favorited contacts.
 *
 * @copyright
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 *
 * @license
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

/**
 * Instantiate the local variables for this controller
 */
var _args = arguments[0] || {}, // Any passed in arguments will fall into this property
	App = Alloy.Globals.App, // reference to the APP singleton object
	users = null,  // Array placeholder for all users
	indexes = [];  // Array placeholder for the ListView Index (used by iOS only);
	
//this sets the variable locaiont (PlayStore vs iTunes) in the instruction text for first page
//var appStoreTarget;
//if (OS_IOS) {
//	strAppStoreTarget="iTunes";
//} else {
//	strAppStoreTarget="Google Play Store";
//};
//var strInstructionText = "Your local data is empty. \n\nIn order to load/refresh data to your device, you must first be connected to the internal Fluor network, either through Junos Pulse Secure (available from " + strAppStoreTarget;
//strInstructionText += ") and using your RSA token, or by connecting to the internal Fluor wireless network FCPD100)";
strInstructionText = "Your local data is empty. \n\nTap Download Data below to load the Fluor phonebook data onto your device.";
$.lblNoRecordsFound.setText(strInstructionText);

/** 
 * Function to inialize the View, gathers data from the flat file and sets up the ListView
 * TODO: Replace this init with one that calls a function to get data from the local sqlite db
 */
function init(){
	
	/**
	 * Access the FileSystem Object to read in the information from a flat file (lib/userData/data.js)
	 * DOCS: http://docs.appcelerator.com/platform/latest/#!/api/Titanium.Filesystem
	 */
	if (Alloy.Globals.verboseLog) { Ti.API.info("Init start"); };
	
	$.listView.sections = null;
	
	//users = null;
	
	// TODO:  GET DATA FROM LOCAL DATA STORE INSTEAD OF DATA.JSON
	//var file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory + "userData/data.json"); 
	
	/**
	 * Populate the users variable from the file this call returns an array
	 */
	//users = JSON.parse(file.read().text).users;
	
	/**
	 * Sorts the `users` array by the LNAME property of the user (leverages UnderscoreJS _.sortBy function)
	 */
	//users = _.sortBy(users, function(user){
		//return user.LNAME;
	//});
	
	/**
	 * IF the users array exists
	 */
	$.viewNoRecordsFound.visible="False";
	$.lblNoLURecordsFound.visible="False";
	$.lblDownloading.visible=false;
	$.lblSearchNotFound.visible=false;
	
	var count = getCount();
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("Init: getCount - count:" + count); };
	
	if(count == 0){
		$.viewNoRecordsFound.visible="True";
		$.listView.removeAllChildren();
		$.listView.hide();
		return;
	} 

	if(users.length == 0) {
		if (Alloy.Globals.verboseLog) { Ti.API.info("rec count:" + users.length); };
		//If only the "No Last Used Records exist" is present, notify user, disallow clicking/swiping
		$.lblNoLURecordsFound.visible="True";
		$.listView.removeAllChildren();
		$.listView.hide();
		return;
	}
		
	if (users[0]==0){
		if (Alloy.Globals.verboseLog) { Ti.API.info("No records found, users[0]:" + users[0]); };
		$.lblSearchNotFound.visible=true;	
		$.listView.removeAllChildren();
		$.listView.hide();
		return;	}
				
	if (Alloy.Globals.verboseLog) { Ti.API.info("user array exists"); };

	/**
	 * Setup our Indexes and Sections Array for building out the ListView components
	 * 
	 */
	indexes = [];
	var sections = [];
	
	/**
	 * Group the data by first letter of last name to make it easier to create 
	 * sections. (leverages the UndrescoreJS _.groupBy function)
	 */
	var userGroups  = _.groupBy(users, function(item){
	 	return item.LNAME.charAt(0);
	});
    
    /**
     * Iterate through each group created, and prepare the data for the ListView
     * (Leverages the UnderscoreJS _.each function)
     */
	_.each(userGroups, function(group){

		/**
		 * Take the group data that is passed into the function, and parse/transform
		 * it for use in the ListView templates as defined in the directory.xml file.
		 */
		var dataToAdd = preprocessForListView(group);

		/**
		 * Check to make sure that there is data to add to the table,
		 * if not lets exit
		 */
		if(dataToAdd.length < 1) return;
		
		
		/**
		 * Lets take the first Character of the LNAME and push it onto the index
		 * Array - this will be used to generate the indices for the ListView on IOS
		 */
		indexes.push({
			index: indexes.length,
			title: group[0].LNAME.charAt(0)
		});

		/**
		 * Create a new ListViewSection, and ADD the header view created above to it.
		 */
		 var section = Ti.UI.createListSection({
		//	headerView: sectionHeader
		});

		/**
		 * Add Data to the ListViewSection
		 */
		section.items = dataToAdd;
		
		/**
		 * Push the newly created ListViewSection onto the `sections` array. This will be used to populate
		 * the ListView 
		 */
		sections.push(section);
		
	});
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("End of init"); };
	
	/**
	 * Add the ListViewSections and data elements created above to the ListView
	 */
	$.listView.sections = sections;
	
	/**
	 * For iOS, we add an event listener on the swipe of the ListView to display the index of the ListView we 
	 * created above. The `sectionIndexTitles` property is only valid on iOS, so we put these handlers in the iOS block.
	 */
	if(OS_IOS) {
		$.wrapper.addEventListener("swipe", function(e){
			if(e.direction === "left"){
				$.listView.sectionIndexTitles = indexes;
			}
			if(e.direction === "right"){
				$.listView.sectionIndexTitles = null;
			}
		});
	}
	//hide no records found message if we loaded records
	$.viewNoRecordsFound.visible="False";
	$.lblNoLURecordsFound.visible="False";
	$.listView.show();
};

/**
 *	Convert an array of data from a JSON file into a format that can be added to the ListView
 * 
 * 	@param {Object} Raw data elements from the JSON file.
 */
var preprocessForListView = function(rawData) {
	
	/**
	 * Using the rawData collection, we map data properties of the users in this array to an array that maps an array to properly
	 * display the data in the ListView based on the templates defined in directory.xml (levearges the _.map Function of UnderscoreJS)
	 */
	return _.map(rawData, function(item) {
		
		/**
		 * Need to check to see if this user item is a favorite. If it is, we will use the `favoriteTemplate` in the ListView.
		 * (leverages the _.find function of UnderscoreJS)
		 */
		var isFavorite = false;
		
		/**
		 * Create the new user object which is added to the Array that is returned by the _.map function. 
		 */
		return {
			// TODO: Change to a single template.  no favorites
			template: "userTemplate",
			properties: {
				searchableText: item.FNAME + ' ' + item.LNAME + ' ' + item.PHONE_NO,
				user: item,
				editActions: [
					{title: isFavorite ? "- Favorite" : "+ Favorite", color: isFavorite ? "#C41230" : "#038BC8" }
				],
				canEdit:false
			},
			userName: {text: item.FNAME ? item.LNAME+", "+item.FNAME : item.LNAME},
			//userName: {text: item.LNAME+", "+item.FNAME},
			userPhone: {text: item.PHONE_NO} 
		};
	});	
};

/**
 * This function handles the click events for the rows in the ListView.
 * We want to capture the user property associated with the row, and pass
 * it into the profile View
 * 
 * @param {Object} Event data passed to the function
 */
function onItemClick(e){
	
	/**
	 * Appcelerator Analytics Call
	 */
	//Ti.Analytics.featureEvent(Ti.Platform.osname+"."+title+".contact.clicked");
	
	/**
	 * Get the Item that was clicked
	 */
	var item = $.listView.sections[e.sectionIndex].items[e.itemIndex];
	
	/**
	 * Open the profile view, and pass in the user data for this contact
	 */
	Alloy.Globals.Navigator.open("profile", item.properties.user);
}

/**
 * This code is only relevant to iOS - to make it cleaner, we are declaring variables, and
 * then assigning them to functions within an iOS Block. On MobileWeb, Android, etc this code block will not
 * exist
 */
var onSearchChange, onSearchFocus, onSearchCancel;

/**
 * Handles the SearchBar OnChange event
 * 
 * @description On iOS we want the search bar to always be on top, so we use the onchange event to tie it back
 * 				to the ListView
 * 
 * @param {Object} Event data passed to the function
 */
onSearchChange = function onChange(e){
	//$.listView.searchText = e.source.value;
	//var users = getData.fetchUsers();

	if (e.source.value.length > 0) {
		var getData = require('flrLoadFTD');
		users = getData.fetchUsersByName(e.source.value);
	} else {
		users = getPhoneData();
	}
	init();
};
	
if(OS_IOS){
	
	/**
	 * Updates the UI when the SearchBar gains focus. Hides the Bookmark icon and shows
	 * the Cancel button.
	 * 
	 * @description We want to use both the bookmark feature and Cancel, but don't want them to show up together (EWW!)
	 * 				so we use the focus event to show the Cancel button and hide the bookmark
	 * 
	 * @param {Object} Event data passed to the function
	 */
	onSearchFocus = function onFocus(e){
			$.searchBar.showCancel = true;
	};
	
	/**
	 * Updates the UI when the Cancel button is clicked within the search bar. Hides the Cancel button 
	 * 
	 * @param {Object} Event data passed to the function
	 */
	onSearchCancel = function onCancel(e){
		if(!_args.restrictToFavorites){
			$.searchBar.showCancel = false;
		}	
		$.searchBar.blur();
		users = getPhoneData();
		init();
	};
	
	/**
	 * handler row actions here.
	 * 
	 *  @param {Object} e  Event data passed to the function
	 */
	function onRowAction(e){
		
		var row = e.section.getItemAt(e.itemIndex);
		var id = row.properties.user.id;
		
			// TODO: removed favorites handling; review if this function is still needed
		
		$.listView.editing = false;
		init();
	}
	
	/* 
	 * Assign `editaction` event listener to ListView 
	 * 
	 * NOTE: Updated to 'editaction' instead of 'rowAction' per
	 * ticket
	 * https://jira.appcelerator.org/browse/TIMOB-19096
	 */
	$.listView.addEventListener("editaction", onRowAction);
}

/**
 * Listen for the refresh event, and re-initialize
 */
Ti.App.addEventListener("refresh-data", function(e){
	if (Alloy.Globals.verboseLog) { Ti.API.info("Directory.js: refresh-data event fired!"); };
	users = getPhoneData();
	init();
});


/**
 * Call contact from the directory list
 * TODO: fix or remove this; 
 */
function callContact(){
	/**
	 * Appcelerator Analytics Call
	 */
	//Ti.Analytics.featureEvent(Ti.Platform.osname+".profile.callContactButton.clicked");
	
	// Get item selected
	var item = $.listView.sections[e.sectionIndex].items[e.itemIndex];
	
	/**
	 * Open the profile view, and pass in the user data for this contact
	 */
	//var userData = item.properties.user;
	
	/**
	 * Before we send the phone number to the platform for handling, lets first verify
	 * with the user they meant to call the contact with an Alert Dialog
	 */
	// TODO:  Remove the prompt after testing.  not expected in native app.
	var dialog = Ti.UI.createAlertDialog({
	    cancel: 0,
	    buttonNames: ['Cancel', 'Ok'],
	    message: "(CALL WILL BE ATTEMPTED) FOR TEST: Are you sure you want to call "+ item.FNAME + " " + item.LNAME +" at "+ item.PHONE_NO
	});
	
	/**
	 * Event Handler associated with clicking the Alert Dialog, this handles the 
	 * actual call to the platform to make the phone call
	 */
	dialog.addEventListener('click', function(e){
		 if (e.index !== e.source.cancel){
	    
	      	if(ENV_DEV){
	     	// IF WE ARE BUILDING FOR DEVELOPMENT
	      		// TOGGLE FAKE NUMBER ON/OFF HERE.......
	      		//Ti.Platform.openURL("tel:+15125551212");
	      	//	Ti.Platform.openURL("tel:"+urlPhone);
	      		Ti.Platform.openURL("tel:"+item.PHONE_NO);
	      	}
	      	// ELSE IF WE ARE BUILDING PRODUCTION - THEN USE THE LISTED NUMBER
	      	else if(ENV_PRODUCTION){
	      	//	Ti.Platform.openURL("tel:"+urlPhone);
	      		Ti.Platform.openURL("tel:"+item.PHONE_NO);
	      	}
	    }  
	});
	
	/**
	 * After everything is setup, we show the Alert Dialog to the User
	 */
	dialog.show();
}

function getPhoneData() {
	var getData = require('flrLoadFTD');
	var strDate = getData.getRefreshDate();
	var datDate = new Date(strDate);
	var strDisplayDate = "";
	if (strDate==""){
		$.refreshDate.text = "Download Data";
		$.deleteAllData.visible=false;
	}
	else{
		strDisplayDate = datDate.getFullYear() + '-' + (datDate.getMonth()+1) + '-' + datDate.getDate();
		
		$.refreshDate.text = "Last Refresh: " + strDisplayDate;
		$.deleteAllData.visible=Alloy.Globals.Development;
	}
	$.version=Alloy.Globals.Version;
	if (Alloy.Globals.verboseLog) { Ti.API.info("Get refresh date: " + $.refreshDate.text); };
	var users = getData.getLastUsed();
	
	return users;
}

function getCount() {
	var loadData = require('flrLoadFTD');
    return loadData.getDirectoryCount();
}

function deleteDirectory() {
      	var loadData = require('flrLoadFTD');
	    loadData.deleteAll();
	    $.viewNoRecordsFound.visible=true;
	    users = getPhoneData();	
	    init();
}

function deleteUserData() {
      	var loadData = require('flrLoadFTD');
	    loadData.deleteLastUsed();
	    users = getPhoneData();
	    init();
}

function disableDownload(){
	$.viewNoRecordsFound.visible=false;
	$.lblNoLURecordsFound.visible=false;
	$.lblDownloading.visible=true;
	$.refreshDate.touchEnabled=false;
	$.deleteAllData.touchEnabled=false;
	$.listView.hide();
	$.refreshDate.text = '';
	$.deleteAllData.visible=false;
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("Download disabled"); };
} 

Ti.App.addEventListener('disableDownload', disableDownload);

function enableDownload(){
	if (Alloy.Globals.verboseLog) { Ti.API.info("Download enabled"); };
	$.lblDownloading.visible=false;
	$.viewNoRecordsFound.visible=false;
	$.lblNoLURecordsFound.visible=false;
	$.refreshDate.touchEnabled=true;
	$.deleteAllData.touchEnabled=true;
	$.listView.show();
	$.refreshDate.text= 'Refresh Now';
	$.deleteAllData.visible=false;
}

Ti.App.addEventListener('enableDownload', enableDownload);

$.listView.addEventListener("scrollstart", function(e){
	if(Ti.Platform.osname == 'android'){
//		if(e.source.toString() != '[object TextField]'){
			Ti.UI.Android.hideSoftKeyboard();
//		}
	} else {
		if(e.source != '[object TiUITextField]'){
			$.searchBar.blur();
		}
	}	
});

function downloadDirectory(e){
	var strMessage = "";
	var versionNumber = Ti.Platform.version;
	var blnAllowDownload = true;
	//for iphones, 
	if (OS_IOS) {
		var versionParts = versionNumber.split(".");
		if (parseInt(versionParts[0]) == 10) {
			if (parseInt(versionParts[1]) < 2) {
				strMessage = strMessage + "Your current OS version of v" + versionNumber + " must be upraded to at least v10.2";
				blnAllowDownload = false;
			}
		}		
	}

	if (blnAllowDownload) {
		strMessage = "In order to download the Fluor phonebook data, the Fluor Phones application needs to make a connection to the Fluor network.  If the connection times out before starting to pull the data, use the back arrow to return to this screen try again.";
		var alertDialog = Titanium.UI.createAlertDialog({ 
				title: 'Confirm Refresh', 
				message: strMessage, 
				buttonNames: ['OK','CANCEL'] 
				}); 
		 	 
			if (Alloy.Globals.verboseLog) { Ti.API.info("Download directory: prompt to refresh"); };
		  	alertDialog.addEventListener('click', function(e){ 
	       	if(e.index == 0) {    
	       		Ti.Analytics.featureEvent('data.download.started');
				Alloy.Globals.Navigator.open("downloader", {displayHomeAsUp:true});
				//Alloy.Globals.Navigator.open("rsa", {displayHomeAsUp:true});
	       	}
	        else {       
	           //alert('go back to your app');           
	        }
	    });
		alertDialog.show();			
	} else {
		var versionDialog = Titanium.UI.createAlertDialog({ 
				title: 'Version', 
				message: strMessage,
				buttonNames: ['OK'] 
				}); 
		 	 
		  	versionDialog.addEventListener('click', function(e){ 
	       	if(e.index == 0) {    	
				//Alloy.Globals.Navigator.open("downloader", {displayHomeAsUp:true});
				//Alloy.Globals.Navigator.open("rsa", {displayHomeAsUp:true});
	       	}
	        else {       
	           //alert('go back to your app');           
	        }
	    });	
		versionDialog.show();
	}
}



if (Alloy.Globals.verboseLog) { Ti.API.info("Reading user data"); };
users = getPhoneData();

/**
 * Initialize View
 */
init();

