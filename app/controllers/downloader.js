// Download database file to local storage

var args = arguments[0] || {};

//Require the https module
// var https = require('appcelerator.https'),
//	 securytManager,
//	 httpClient;
	
//set variable as reference to the Module object
//var securityManager = https.createX509CertificatePinningSecurityManager([
//	 {
//	 url: Alloy.Globals.certURL,
//	 serverCertificate: "fdnet.cer"		
//	 }
//]);

var count = 50000;
var batch = count / Alloy.Globals.batchSize;

var i = 0;

/* Create a progress bar */
if (Alloy.Globals.verboseLog) { Ti.API.info("Create a progress bar"); };
var ind=Titanium.UI.createProgressBar({	
	width:200,
	height:100,
	min:0,
	max:Alloy.Globals.batchSize,
	value:0,
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	top:50,
	message:'Downloading data',
	font:{fontSize:12, fontWeight:'bold'},
	color:Alloy.Globals.FluorBlue
});

/**
 * fade in after the view rendered
 */
$.downwin.addEventListener("postlayout", function(e){
	$.downwin.animate({
		opacity: 1.0,
		duration: 250,
		curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
	});
});

if (Alloy.Globals.verboseLog) { Ti.API.info("Add progress bar to downloader window and show it"); };
$.downwin.add(ind);
ind.show();

// clear db
var loadData = require('flrLoadFTD');
loadData.deleteAll();

var batch_downloaded = function() {
	
	if (i>Alloy.Globals.batchSize){
		if (Alloy.Globals.verboseLog) { Ti.API.info("Download - start: " + batch * i + " count: " + 10000); };
		var restURL = Alloy.Globals.sourceURL + Alloy.Globals.viewName + "?systemcolumns=0x0010&start=" + batch * i +"&count=100000";
	} else {
		if (Alloy.Globals.verboseLog) { Ti.API.info("Download - start: " + batch * i + " count: " + batch); };
		var restURL = Alloy.Globals.sourceURL + Alloy.Globals.viewName + "?systemcolumns=0x0010&start=" + batch * i +"&count=" + batch;
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("*** batch_download: restURL:" + restURL); };
	xhr.open('GET',restURL);
	xhr.send();

	if (Alloy.Globals.verboseLog) { Ti.API.info("batch " + i + " downloaded"); };
	if (Alloy.Globals.verboseLog) { Ti.API.info("count: " + count + " batch: " + batch); };
	i++;
	ind.value = i;
};

Ti.App.addEventListener('batch_downloaded', batch_downloaded);


var file_downloaded = function() {
	ind.message = 'Download complete';
	Ti.Analytics.featureEvent('data.download.finished');

	if (Alloy.Globals.verboseLog) { Ti.API.info("file download complete"); };	
	//Alert user download has finished, close window on confirmation
	var alertDialog = Titanium.UI.createAlertDialog({ 
			title: 'Download Complete', 
			message: 'The Fluor Phonebook has been downloaded onto your device', 
			buttonNames: ['OK'] 
			}); 
	alertDialog.addEventListener('click', function(e){ 
	    if(e.index == 0) {    	
			$.downwin.close();
			//Alloy.Globals.Navigator.open("directory", {displayHomeAsUp:true});
	    }
	    else {       
	        $.downwin.close();   
	        //Alloy.Globals.Navigator.open("directory", {displayHomeAsUp:true});       
	    }
	});
	alertDialog.show();
};

Ti.App.addEventListener('file_downloaded', file_downloaded);

if (Alloy.Globals.verboseLog) { Ti.API.info("generate the httpclient and send the get request"); };

function getCount(e){
	var responseString = JSON.parse(e.replace(/@siblings/g,'siblings'));
	if (responseString.length > 0){
		count = responseString[0].siblings;
		var float_value = String(count / Alloy.Globals.batchSize);
		var index = float_value.indexOf('.');
        if (index > 0) {
            batch = float_value.substr(0, index);
        }
        else {
            batch = float_value;
        }
	} else {
		count = 50000;
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("getCount count: " + count); };
	return count;
}

var xhr = Titanium.Network.createHTTPClient({
	 validateSecureCertificate : true,
//	 securityManager: https.createX509CertificatePinningSecurityManager([
//	 {
//	 	url: Alloy.Globals.certURL,
//		 serverCertificate: "certnew.cer"		
//		 }
//	 ]),

	
	onload: function() {
		var tls = /TLS\s\d\.\d/g.exec(this.responseText);
		if (tls) {
			Ti.API.info('TLS: ' + tls[0]);
		}
		if (Alloy.Globals.verboseLog) {Ti.API.info("Received text: " + this.responseText); };
		var maxCount = Alloy.Globals.batchSize + 2;
		if (i==0){
			getCount(this.responseText);
			if (Alloy.Globals.verboseLog) { Ti.API.info("Directory count: " + count); };
			Ti.App.fireEvent('batch_downloaded', batch_downloaded);
		} else {
			if (i < maxCount) {
				Ti.App.fireEvent('batch_downloaded', batch_downloaded);
				loadData.commitData(this.responseText);
			} else {
				var moment = require('alloy/moment');
				loadData.saveRefreshDate(moment().format('MM/DD/YYYY'));				
				Ti.App.fireEvent('file_downloaded', file_downloaded);
				Ti.App.fireEvent('enableDownload', null);
				Ti.App.fireEvent('refresh-data', null);
				Ti.App.removeEventListener('batch_downloaded', batch_downloaded);
				Ti.App.removeEventListener('file_downloaded', file_downloaded);
			}
		}
	},
    // function called when an error occurs, including a timeout
    onerror : function(e) {
        Ti.API.debug(e.error);
		Titanium.API.info("JSON ERROR: " + JSON.stringify(securityManager));
		Titanium.API.info("ValidateSecureCertificate: : " + xhr.validateSecureCertificate);
        alert('Network error!\n\nYour device was unable to download/refresh the Fluor Phonebook.  Please make sure you have a strong connection to the Fluor network, tap the back arrow to return to the previous page, and tap Continue Download again.  In some cases it may be necessary to exit the application and start again.');
		if (Alloy.Globals.verboseLog) { Ti.API.info("error: " + e.error); };        
		loadData.deleteAll();
        ind.value = Alloy.Globals.batchSize;
        ind.message = 'Error downloading data...';
		Ti.App.fireEvent('enableDownload', null);
		Ti.App.fireEvent('refresh-data', null);
		Ti.App.removeEventListener('batch_downloaded', batch_downloaded);
		Ti.App.removeEventListener('file_downloaded', file_downloaded);        
    },
	timeout: 20000,
    //Set the securityManager in order to use HTTPS
    
    //securityManager: securityManager
});

Ti.API.info("ValidateSecureCertificate: : " + xhr.validateSecureCertificate);

// Get data
var restURL = Alloy.Globals.sourceURL + Alloy.Globals.viewName + "?systemcolumns=0x0010&start=1&count=1";
if (Alloy.Globals.verboseLog) { Ti.API.info("restURL:" + restURL); };

if (Alloy.Globals.verboseLog) { Ti.API.info("about to fire disableDownload event"); };
Ti.App.fireEvent('disableDownload', null);
if (Alloy.Globals.verboseLog) { Ti.API.info("disableDownload even fired"); };


if (Alloy.Globals.verboseLog) { Ti.API.info("*** bottom of downloader.js: restURL:" + restURL); };
//if (Alloy.Globals.verboseLog) { Ti.API.info("TLS version: " + Ti.Network.HTTPClient.tlsVersion); };
xhr.open('GET',restURL);
xhr.send();

/**
 * Closes the Window
 */
function closeWindow(){
	$.downwin.close();
}

