// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
Alloy.Globals.Version="v1.10";
Alloy.Globals.Development="false";

// ===================================  SWITCH FOR ENVIRONMENT  =====================================
//
//================================  Enable below for DEV environment  ===============================
//Alloy.Globals.prefixURL = "http://fclndev20.fdnet.com";
//Alloy.Globals.certURL = "http://fclndev20.fdnet.com";
//================================  Enable below for PROD environment  ==============================
Alloy.Globals.prefixURL = "https://www.fdnet.com";
//Alloy.Globals.prefixURL = "https://10.50.19.59";
Alloy.Globals.certURL = "https://www.fdnet.com";
//===================================================================================================
//Alloy.Globals.viewName = "/directoryloader";  //<----  old, use for Appcelerator https testing (can be removed when completed)
Alloy.Globals.viewName = "/directoryloader02";  //<----  new, use going forward

Alloy.Globals.sourceURL = Alloy.Globals.prefixURL + "/FDGlobal/WORKGRPS/FM/NET_TD.NSF/api/data/collections/name";
Alloy.Globals.webViewURL = Alloy.Globals.prefixURL + "/fdglobal/workgrps/fm/NET_TD.NSF/FluorPhonesAuth?OpenPage";

// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Globals.deviceWidth=Ti.Platform.displayCaps.platformWidth;
Alloy.Globals.deviceHeight=Ti.Platform.displayCaps.platformHeight;
Alloy.Globals.listViewHeight=Alloy.Globals.deviceHeight-65;
Alloy.Globals.WCWidth=0.5*Alloy.Globals.deviceWidth;
Alloy.Globals.WCHeight=0.1*Alloy.Globals.deviceHeight;
Alloy.Globals.osname=Ti.Platform.osname;
//Alloy.Globals.version=Ti.Platform.version;

Alloy.Globals.animationDuration=250;
Alloy.Globals.itemsPerSearch='30';
Alloy.Globals.verboseLog = true;
Alloy.Globals.batchSize=30;

Alloy.Globals.DatabaseName = "FluorTelDir";
Alloy.Globals.FluorBlue="#00467f";

function setupDB() {
	// TODO: After DB is in place/working:  try 'WITHOUT ROWID' at the end of the execute to see if it's more performant
	if (Alloy.Globals.verboseLog) { Ti.API.info("alloy.js: setupDB - start"); };
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	
	db.execute('CREATE TABLE if NOT EXISTS FTDContacts ("docUNID" TEXT PRIMARY KEY, "LNAME" TEXT, "FNAME" TEXT, "PHONE_NO" TEXT, "OFFICE" TEXT, "LOC" TEXT, "IODC" TEXT, "FAX_No" TEXT, "FullName" TEXT, "ModifiedDate" TEXT, "ModifiedBy" TEXT)');
	db.execute('CREATE TABLE if NOT EXISTS UsedContacts ("docUNID" TEXT PRIMARY KEY, "DateLastUsed" TEXT)');
	db.execute('CREATE TABLE if NOT EXISTS Parameters ("param_name" TEXT PRIMARY KEY, "value" TEXT)');

	db.close();
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("alloy.js: setupDB - end"); };
}; 

setupDB();
